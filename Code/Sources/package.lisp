(pushnew '("application" . "json") drakma:*text-content-types* :test #'equal)
(pushnew :lisp-strikes-twice.sources *features*)

(defpackage :lisp-strikes-twice.sources
  (:use :cl)
  (:import-from #:snakes   #:defgenerator #:generator-stop #:yield #:with-yield #:do-generator)
  (:import-from #:drakma   #:http-request)
  (:import-from #:cl-ppcre #:do-matches-as-strings #:create-scanner)
  (:import-from #:lquery   #:$)
  (:import-from #:plump    #:parse)
  (:import-from #:cl-bloom #:add #:memberp #:make-filter)
  (:import-from #:split-sequence #:split-sequence)
  (:import-from #:lisp-strikes-twice.utilities #:define-dork)
  (:export #:*ip-regexp* #:*http-proxy-dork*
           #:bing-scrape
           #:bing-scrape-proxies
           #:scrape-page-for-proxies
           #:http-proxy
           #:http-proxy-ip
           #:http-proxy-port
           #:scanner-search
           #:shodan-state
           #:censys-state
           #:network-tuple->url
           #:network-tuple-ip
           #:network-tuple-port
           #:scan-result
           #:scan-result-ip
           #:scan-result-port
           #:scan-result-source))
