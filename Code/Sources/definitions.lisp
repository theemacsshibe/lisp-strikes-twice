(in-package :lisp-strikes-twice.sources)

(defclass ip-port-tuple ()
  ((ip   :initarg :ip   :reader network-tuple-ip)
   (port :initarg :port :reader network-tuple-port)))

(defclass http-proxy (ip-port-tuple)
  ((ip   :initarg :ip   :reader http-proxy-ip)
   (port :initarg :port :reader http-proxy-port)))

(defmethod print-object ((proxy http-proxy) stream)
  (print-unreadable-object (proxy stream)
    (format stream "HTTP-Proxy ~a:~d"
            (http-proxy-ip proxy)
            (http-proxy-port proxy))))

(defclass scan-result (ip-port-tuple)
  ((ip     :initarg :ip     :reader scan-result-ip)
   (port   :initarg :port   :reader scan-result-port)
   (source :initarg :source :reader scan-result-source)))

(defmethod print-object ((result scan-result) stream)
  (print-unreadable-object (result stream)
    (format stream "Scan result ~a:~d from ~:(~a~)"
            (scan-result-ip result)
            (scan-result-port result)
            (scan-result-source result))))

(defmethod update-instance-for-different-class :before ((old scan-result)
                                                        (new http-proxy)
                                                        &key)
  (setf (slot-value new 'ip)   (slot-value old 'ip)
        (slot-value new 'port) (slot-value old 'port)))

(defclass scanner () ())

(defgeneric scanner-search (scanner query))
(defgeneric network-tuple->url (tuple schema))
(defgeneric network-tuple->instance (tuple instance-type))

(defmethod network-tuple->url ((tuple ip-port-tuple) schema)
  (format nil "~a://~a:~d"
          schema
          (network-tuple-ip tuple)
          (network-tuple-port tuple)))

(defmethod network-tuple->instance ((tuple ip-port-tuple) instance-type schema)
  (make-instance instance-type
                 :base-url (network-tuple->url tuple schema)))
