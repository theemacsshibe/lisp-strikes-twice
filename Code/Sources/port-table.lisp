(in-package :lisp-strikes-twice.sources)

(defvar *port-hash-table* (make-hash-table))

(defmacro define-network-services (&rest services)
  `(setf ,@(loop for (service-name . service-ports) in services
              appending `((gethash ',service-name *port-hash-table*) ',service-ports))))

(define-network-services
    (:echo 7)
    (:chargen 19)
    (:ftp 21)
    (:ssh 22)
    (:telnet 23)
    (:smtp 25)
    (:whois 43)
    (:gopher 70)
    (:http 80 8008 8080)
    (:sftp 115)
    (:sql 118 156)
    (:nntp 119)
    (:ntp 123)
    (:imap 143)
    (:irc 194)
    (:https 443 8443)
    (:rexec 512)
    (:rlogin 513)
    (:rsh 514)
    (:lpd 515)
    (:cups 631))
  
(defun service-uses-port-p (port name)
  (and (member port (gethash name *port-hash-table*)) t))

(defun service-port-uses (name)
  (gethash name *port-hash-table*))
