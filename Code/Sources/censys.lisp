(in-package :lisp-strikes-twice.sources)

(defclass censys-state ()
  ((api-key :initarg :api-key :reader censys-api-key)
   (secret  :initarg :secret  :reader censys-secret)))

(defun %get-censys-page (scanner query page-n)
  (jsown:val
   (jsown:parse
    (http-request "https://censys.io/api/v1/search/ipv4"
                  :method :post
                  :content-type "application/json"
                  :content (jsown:to-json `(:obj
                                            ("query" . ,query)
                                            ("page" . ,page-n)))
                  :basic-authorization (list (censys-api-key scanner)
                                             (censys-secret  scanner))))
   "results"))

(defun %censys-json->scan-results (result)
  (let ((ip (jsown:val result "ip")))
    (loop for protocol in (jsown:val result "protocols")
       for number-part = (first (split-sequence #\Slash protocol))
       collect (make-instance 'scan-result
                              :source :censys
                              :port (parse-integer number-part)
                              :ip ip))))

(declaim (notinline %make-censys-generator))
(defgenerator %make-censys-generator (scanner query)
  (loop
     for page from 1
     do (let ((json (%get-censys-page scanner query page)))
          (dolist (ip json)
            (dolist (result (%censys-json->scan-results ip))
              (yield result))))))

(defmethod scanner-search ((scanner censys-state) query)
  (%make-censys-generator scanner query))
