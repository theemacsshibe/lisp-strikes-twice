(in-package :lisp-strikes-twice.sources)

(defclass shodan-state ()
  ((api-key :initarg :api-key :reader shodan-api-key)))

(defun %get-shodan-page (scanner query page-n)
  (jsown:val 
   (jsown:parse
    (http-request "https://api.shodan.io/shodan/host/search"
                  :parameters `(("key" . ,(shodan-api-key scanner))
                                ("page" . ,(princ-to-string page-n))
                                ("minify" . "true")
                                ("query" . ,query))))
   "matches"))

(defun %shodan-json->scan-result (result)
  (make-instance 'scan-result
                 :source :shodan
                 :port (jsown:val result "port")
                 :ip (jsown:val result "ip_str")))

(declaim (notinline %make-shodan-generator))
(defgenerator %make-shodan-generator (scanner query)
  (loop
     for page from 1
     for result in (%get-shodan-page scanner query page)
     do (yield (%shodan-json->scan-result result))))

(defmethod scanner-search ((scanner shodan-state) query)
  (%make-shodan-generator scanner query))
