(in-package :lisp-strikes-twice.sources)

(defvar *ip-regexp* (create-scanner "\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b(.){1,16}\\d{1,5}"))
(defvar *http-proxy-dork* "+\":8080\" +\":3128\" +\":80\" filetype:txt")
(define-dork http-proxy "+\":8080\" +\":3128\" +\":80\" filetype:txt")

(defun scan-for-ip (string)
  (let ((left-point (position-if-not (lambda (c)
                                       (or (digit-char-p c)
                                           (char= c #\.)))
                                     string))
        (right-point (position-if-not #'digit-char-p string :from-end t)))
    (when (and left-point right-point)
      (let ((ip   (subseq string 0 left-point))
            (port (parse-integer string :start (1+ right-point) :junk-allowed t)))
        (when (and ip port)
          (make-instance 'http-proxy :ip ip :port port))))))

(defgenerator scrape-page-for-proxies (url &key (timeout 5))
  (ignore-errors
    (let ((page (http-request url :connection-timeout timeout)))
      (when (typep page 'string)
        (do-matches-as-strings (match *ip-regexp* page)
          (let ((match (scan-for-ip match)))
            (when match
              (yield match))))))))

(defun bing-search-urls (query &optional (first 1))
  (ignore-errors
    (let ((page (parse 
                 (http-request "https://bing.com/search/"
                               :parameters `(("q" . ,query)
                                             ("first" . ,(princ-to-string first)))))))
      (coerce (lquery:$ page "li > h2 > a" (attr :href)) 'list))))

(defgenerator bing-scrape (query)
  (loop
     for first-n = 1 then (+ (length urls) first-n)
     for urls = (bing-search-urls query first-n)
     do (dolist (url urls)
          (yield url))))

(defgenerator bing-scrape-proxies (query &key (timeout 5))
  (let ((bloom (make-filter :false-drop-rate 1/1000)))
    (do-generator (url (bing-scrape query))
      (do-generator (proxy (scrape-page-for-proxies url :timeout timeout))
        (let ((name (princ-to-string proxy)))
          (unless (memberp bloom name)
            (add bloom name)
            (yield proxy)))))))
