(in-package :lisp-strikes-twice.utilities)

(defmacro <- (&rest things)
  "A right-way-around version of cl-arrows's ->."
  (labels ((helper (things)
             (if (null (rest things))
                 (first things)
                 (if (symbolp (first things))
                     (list (first things) (helper (rest things)))
                     `(,@(first things) (helper (rest things)))))))
    (helper things)))

(defmacro map<- (mapper &rest things)
  "A mapping right-way-around version of cl-arrows's ->."
  (let ((variable-name (gensym "LAMBDA-VAR")))
    (labels ((helper (things)
               (if (null (rest things))
                   variable-name
                   (cond
                     ((symbolp (first things))
                      (list (first things) (helper (rest things))))
                     ((member '_ (first things))
                      (substitute (helper (rest things)) '_ (first things)))
                     (t
                      `(,@(first things) (helper (rest things))))))))
                       
      `(,@(if (consp mapper) mapper (list mapper))
          (lambda (,variable-name) ,(helper things))
          ,(first (last things))))))
