(defpackage :lisp-strikes-twice.utilities
  (:use :cl)
  (:export #:define-dork
           #:get-dorks
           #:define-network-services
           #:service-uses-port-p
           #:service-port-uses

           #:map<-
           #:<-))
