# Lisp Strikes (Not Once But) Twice

*Lisp Strikes Twice* is an exploit-doing program, similar to something like
Metasploit but more expressive and more productive.

The aim of LST is to make automating pentesting as homogenous as possible; 
results from one scanner or another scanner both produce results that are 
exactly the same in reading, and the same methods are applicable to two models 
of device that have a similar exploit. 

## Benefits for developers

Developers of LST methods have access to common boilerplate through CLOS
inheritance; shell exploit writers don't have to write their own methods to
test if a target is exploitable, and pretty printers and other utilities come
free as well.

## What's already done?

- Interfaces for Shodan and Censys are completed, which use
  [snakes](https://github.com/BnMcGn/snakes) generators. These generate
  SCAN-RESULT instances, which also inherit interfaces from IP-PORT-TUPLE.
- Bing dorking is also present using
  [lquery](https://github.com/Shinmera/lquery), which can find URLs and also
  can dork for HTTP proxies using a fairly fancy text scanner that works
  very well on both HTML and plaintext formats, creating HTTP-PROXY instances,
  which also have IP-PORT-TUPLE interfaces.
- Lookup tables for dorks and ports are also provided, and can be used inside
  exploit code to provide information on how to find targets for an exploit.
  
- Fairly simple shell exploits over HTTP for routers have also been created,
  such as for the DLink DIR-300 and Netgear DGN1000. One exploit has also been
  written for the generic "MVPower" DVR brand, but I haven't had any luck with
  testing that one.
- As mentioned in *Benefits for developers*, boilerplate for writing shell
  targets has been made, including a generic SHELL-TARGET-WORKS-P function for
  testing the presence of exploits.
