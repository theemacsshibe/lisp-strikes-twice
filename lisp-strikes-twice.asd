(asdf:defsystem lisp-strikes-twice
  :depends-on (lisp-strikes-twice-utilities
               lisp-strikes-twice-sources
               lisp-strikes-twice-exploits)
  :components ((:file "package")))
