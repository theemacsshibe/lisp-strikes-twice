(defpackage :lisp-strikes-twice
  (:use :cl
        :lisp-strikes-twice.sources
        :lisp-strikes-twice.exploits
        :lisp-strikes-twice.utilities
        :snakes :drakma)
  (:nicknames :lst))
